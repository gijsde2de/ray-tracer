#include "precomp.h" // include (only) this in every .cpp file

Camera::Camera(vec3 o, vec3 d, float f) {
	origin = o;
	direction = d;
	fov = f;
	Up = vec3(0, -1, 0);
	calculateIntrinsics();
}

Ray Camera::GetRay(float u, float v) {
	vec3 dir = normalize(direction + p0 + u * (p1 - p0) + v * (p2 - p0) - origin);
	return Ray(origin, dir);
}

void Camera::MoveGlobal(vec3 delta) {
	origin += delta;
	calculateIntrinsics();
}

void Camera::MoveLocal(vec3 delta) {
	origin += delta * direction;
	calculateIntrinsics();
}

void Camera::calculateIntrinsics() {
	vec3 c = origin + fov * direction;
	p0 = c + Up - direction.cross(Up) + direction;// vec3(-1, -1, 1);
	p1 = c + Up + direction.cross(Up) + direction;// vec3(1, -1, 1);
	p2 = c - Up - direction.cross(Up) + direction;// vec3(-1, 1, 1);

}