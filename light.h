#pragma once

namespace Tmpl8 {
	class Light
	{
	public:
		vec3 color;
		vec3 position;
		float intensity;
		Light(vec3 p, vec3 c, float i) { color = c; position = p; intensity = i; }
	private:

	};

}; // namespace Tmpl8