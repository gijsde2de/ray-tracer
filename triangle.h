#pragma once

namespace Tmpl8 {
	class Triangle
	{
	public:
		Triangle(vec3 point, vec3 edge1, vec3 edge2);
		bool Intersect(Ray& ray);
		vec3 e1;
		vec3 e2;
		vec3 p1;
		float* center;
	};
}