#pragma once

namespace Tmpl8 {
	class Ray
	{
	public:
		Ray(vec3 o, vec3 d, bool sR = false) {
			origin = o;
			direction = d;
			invDir = vec3(1 / direction.x, 1 / direction.y, 1 / direction.z);
			t = FLT_MAX;
			shadowRay = sR;
		}
		vec3 origin, direction;
		vec3 invDir;
		float t;
		bool shadowRay;

		void Transform(mat4 trans)
		{
			/*cout << trans[0] << " " << trans[1] << " " << trans[2] << " " << trans[3] << "\n";
			cout << trans[4] << " " << trans[5] << " " << trans[6] << " " << trans[7] << "\n";
			cout << trans[8] << " " << trans[9] << " " << trans[10] << " " << trans[11] << "\n";
			cout << trans[12] << " " << trans[13] << " " << trans[14] << " " << trans[15] << "\n\n";
			cout << origin.x << " " << origin.y << " " << origin.z << "\n";
			
			cout << origin.x << " " << origin.y << " " << origin.z << "\n\n";*/
			if (!shadowRay) {
				origin = (vec4(origin, 1) * trans).xyz;
				direction = (vec4(direction, 0) * trans).xyz;
			}
		}

	private:
		
	};

}; // namespace Tmpl8