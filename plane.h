#pragma once

namespace Tmpl8 {
	class Plane
	{
	public:
		Plane(vec3 n, float d) { normal = n; distance = d; }
		vec3 normal;
		float distance;

		bool Intersect(Ray& ray);
	};
}