#include "precomp.h" // include (only) this in every .cpp file

bool Plane::Intersect(Ray& ray)
{
	float d = dot(ray.direction, normal);
	if (d > -FLT_EPSILON && d < FLT_EPSILON) return false;

	float t = -(dot(ray.origin, normal) + distance) / d;
	if (t >= ray.t || t <= 0) return false;

	ray.t = t;
	return true;
}
