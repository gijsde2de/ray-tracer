#define TINYOBJLOADER_IMPLEMENTATION

#include "precomp.h" // include (only) this in every .cpp file

Scene::Scene() {
	materials.emplace_back(Material(vec3(1, 0, 0), 0.5f, Material::SPECULAR));
	materials.emplace_back(Material(vec3(1, 1, 1), 0.f, Material::DIFFUSE));

	lights.emplace_back(Light(vec3(0, -10, 0), vec3(1, 1, 1), 1000));
	//lights.emplace_back(Light(vec3(0, -10, 500), vec3(1, 1, 1), 1000));

	staticBVH = new BVH();
	rigidBVH = new BVH();
	deformBVH = new BVH();
	animationBVH = new BVH();

	loadObjFile("deer", vec3(0, 0, 20), *staticBVH, 0.002);
	loadObjFile("deer", vec3(-5, 0, 20), *rigidBVH, 0.002);
	loadObjFile("deer", vec3(0, -3, 20), *animationBVH, 0.002);
	loadObjFile("deer", vec3(5, 0, 20), *deformBVH, 0.002);
	
	staticBVH->Construct(30);
	rigidBVH->Construct(30);
	deformBVH->Construct(30);
	animationBVH->Construct(30, 10);

	bvhs.push_back(rigidBVH);
	bvhs.push_back(deformBVH);
	bvhs.push_back(animationBVH);
	bvhs.push_back(staticBVH);

	// Construct top level BVH
	Construct();
}

void Scene::loadObjFile(string filename, vec3 offset, BVH& bvh, float scale) {
	string inputfile = "assets/"+filename+".obj";
	tinyobj::attrib_t attrib;
	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> materials;

	string err;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, inputfile.c_str());

	if (!err.empty()) { // `err` may contain warning message.
		printf(err.c_str());
	}

	if (!ret) {
		exit(1);
	}

	// Loop over shapes
	for (size_t s = 0; s < shapes.size(); s++) {
		// Loop over faces(polygon)
		size_t index_offset = 0;
		for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
			int fv = shapes[s].mesh.num_face_vertices[f];
			tinyobj::index_t idx = shapes[s].mesh.indices[index_offset];
			vec3 point1 = vec3(attrib.vertices[3 * idx.vertex_index + 0], -attrib.vertices[3 * idx.vertex_index + 1], attrib.vertices[3 * idx.vertex_index + 2]) * scale;
			idx = shapes[s].mesh.indices[index_offset + 1];
			vec3 point2 = vec3(attrib.vertices[3 * idx.vertex_index + 0], -attrib.vertices[3 * idx.vertex_index + 1], attrib.vertices[3 * idx.vertex_index + 2]) * scale;
			idx = shapes[s].mesh.indices[index_offset + 2];
			vec3 point3 = vec3(attrib.vertices[3 * idx.vertex_index + 0], -attrib.vertices[3 * idx.vertex_index + 1], attrib.vertices[3 * idx.vertex_index + 2]) * scale;
			Triangle t = Triangle(point1 + offset, point3 - point1, point2 - point1);
			
			bvh.AddTriangle(t, 0);

			index_offset += fv;

			// per-face material
			shapes[s].mesh.material_ids[f];
		}
	}
	cout << "Done loading " + filename + " file \n";
}

vec3 Scene::Trace(Ray r, uint depth = 0) {
	vec3 color = vec3(0, 0, 0);
	if (++depth > MAX_BOUNCES) return color;
	Intersection intersection;
	intersection.material = -1;
	
	if (!intersect(r, intersection, nodes.back())) { return color; }

	Material mat = materials[intersection.material];
	switch (mat.type)
	{
		case Material::DIFFUSE:
			return mat.color * directIllumination(intersection);
		case Material::MIRROR:
			reflect(r, intersection);
			return mat.color * Trace(r, depth);
		case Material::SPECULAR:
			return mat.color * directIllumination(intersection) * (1 - mat.specularity) + specularIllumination(r, intersection) * mat.specularity;
	}

	return color;
}

void Scene::Update()
{
	mat4 trans = rigidBVH->GetTransformation();
	trans[3] -= 0.01;
	//mat4 rot = mat4().rotatey(0.001);
	//trans = rot * trans;
	
	/*cout << trans[0] << " " << trans[1] << " " << trans[2] << " " << trans[3] << "\n";
	cout << trans[4] << " " << trans[5] << " " << trans[6] << " " << trans[7] << "\n";
	cout << trans[8] << " " << trans[9] << " " << trans[10] << " " << trans[11] << "\n";
	cout << trans[12] << " " << trans[13] << " " << trans[14] << " " << trans[15] << "\n\n";
	*/

	rigidBVH->SetTransformation(trans);

	counter++;
	float factor = 1.04f;

	vector<pair<Triangle, size_t>>* triangles = &deformBVH->triangles;
	for (auto i = triangles->begin(); i != triangles->end(); i++)
	{
		//cout << i->first.p1.x << " " << i->first.p1.y << " " << i->first.p1.z << " to ";
		//cout << counter << " ";
		vec3 p1 = i->first.p1 - vec3(5, 0, 20);
		if (counter > 25)
		{
			i->first.p1 = p1 * (1 / factor) + vec3(5, 0, 20);
			i->first.e1 *= (1 / factor);
			i->first.e2 *= (1 / factor);
		}
		else {
			i->first.p1 = p1 * factor + vec3(5, 0, 20);
			i->first.e1 *= factor;
			i->first.e2 *= factor;
		}
		//cout << i->first.p1.x << " " << i->first.p1.y << " " << i->first.p1.z << " \n";
	}

	if (counter == 49)
		counter = 0;
}

void Scene::reflect(Ray& r, Intersection i)
{
	r.direction = r.direction - 2.f * dot(r.direction, i.normal) * i.normal;
	r.origin = i.position + r.direction * 0.001;
	r.t = FLT_MAX;
}

vec3 Scene::specularIllumination(Ray r, Intersection i) {
	vec3 v = -r.direction;
	vec3 result = vec3(0,0,0);
	for (auto l = lights.begin(); l != lights.end(); ++l)
	{
		vec3 h = 0.5 * normalize(l->position - i.position) + 0.5 * v;
		float specular = max(0, pow(dot(i.normal, h), 10));
		result += specular * l->color;
	}

	return result;
}

vec3 Scene::directIllumination(Intersection i) {
	vec3 result = vec3(0, 0, 0);
	for (auto l = lights.begin(); l != lights.end(); ++l)
	{
		// Cast a shadow ray to see if it even reaches the lights
		Ray shadow = Ray(i.position, l->position - i.position, true);
		shadow.t = shadow.direction.length();
		shadow.direction *= (1 / shadow.t);
		shadow.origin += shadow.direction * 0.001;

		if (intersect(shadow, nodes.back())) continue;

		// Now illuminate it
		float lum = dot(shadow.direction, i.normal);
		float att = shadow.t / l->intensity + 1;

		result += l->color * ((lum < 0.0f ? 0.0f : lum) / (att * att));
	}

	return result;
}

// -----------------------------------------------------------
// Top-Level BVH construction
// -----------------------------------------------------------
void Scene::Construct()
{
	if (bvhs.size() == 0) return;

	// Clear previous tree and create new list for all BVHs
	// This list is temporary to keep track of nodes to be combined
	nodes.clear();
	vector<unsigned int> list;

	for (auto i = bvhs.begin(); i != bvhs.end(); ++i)
	{
		Node node;
		node.bounds = (*i)->root->bounds;
		node.bvh = *i;
		
		list.push_back(nodes.size());
		nodes.push_back(node);
	}

	// Construct Top-Level BVH
	unsigned int Aidx = 0;
	unsigned int Bidx = bestMatch(list, list.front());

	while (list.size() > 1)
	{
		unsigned int Cidx = bestMatch(list, list[Bidx]);

		if (Aidx == Cidx)
		{
			// Create new parent node
			Node node;
			node.bvh = nullptr;
			node.left = list[Aidx];
			node.right = list[Bidx];
			node.bounds = extendBounds(nodes[node.left].bounds, nodes[node.right].bounds);

			// Erase nodes from temporary list
			erase(list, Aidx);
			if (Bidx == list.size()) Bidx = Aidx;
			erase(list, Bidx);

			// Add node to lists
			Aidx = list.size();
			list.push_back(nodes.size());
			nodes.push_back(node);

			Bidx = bestMatch(list, list.back());
		}
		else
		{
			Aidx = Bidx;
			Bidx = Cidx;
		}
	}
}

AABB Scene::extendBounds(AABB a, AABB b) {
	AABB aabb;
	aabb.min.x = min(a.min.x, b.min.x);
	aabb.min.y = min(a.min.y, b.min.y);
	aabb.min.z = min(a.min.z, b.min.z);
	aabb.max.x = max(a.max.x, b.max.x);
	aabb.max.y = max(a.max.y, b.max.y);
	aabb.max.z = max(a.max.z, b.max.z);

	return aabb;
}

unsigned int Scene::bestMatch(vector<unsigned int>& list, unsigned int index)
{
	float bestSAH = FLT_MAX;
	unsigned int best = 0;

	for (auto i = list.begin(); i != list.end(); ++i)
	{
		if (*i != index) {
			// Find the two elements for which the AABB has the smallest surface area
			float SAH = extendBounds(nodes[index].bounds, nodes[*i].bounds).SurfaceArea();

			if (SAH < bestSAH)
			{
				bestSAH = SAH;
				best = std::distance(list.begin(), i);
			}
		}
	}

	return best;
}

void Scene::erase(vector<unsigned int>& list, unsigned int idx)
{
	unsigned int last = list.size() - 1;
	if (idx != last) list[idx] = list[last];
	list.pop_back();
}

bool Scene::intersect(Ray& ray, Node& node)
{
	// Leaf node, just traverse the sub-BVH
	if (node.bvh != NULL) return node.bvh->Intersect(ray);

	// It does not intersect this node
	if (!node.bounds.Intersect(ray)) return false;

	// Intersect the child nodes, we can early exit here
	if (intersect(ray, nodes[node.left]))
		return true;
	if (intersect(ray, nodes[node.right]))
		return true;

	return false;
}

bool Scene::intersect(Ray& ray, Intersection& intersection, Node& node)
{
	bool ret = false;

	// Leaf node, just traverse the sub-BVH
	if (node.bvh != NULL) return node.bvh->Intersect(ray, intersection);

	// It does not intersect this node
	if (!node.bounds.Intersect(ray)) return false;

	// Intersect the child nodes
	if (intersect(ray, intersection, nodes[node.left]))
		ret = true;
	if (intersect(ray, intersection, nodes[node.right]))
		ret = true;

	return ret;
}

void Scene::Refit()
{
	for (auto i = nodes.rbegin(); i != nodes.rend(); i++)
	{
		if (i->bvh != nullptr) i->bounds = i->bvh->GetAABB();
		else  i->bounds = extendBounds(nodes[i->left].bounds, nodes[i->right].bounds);
	}
}