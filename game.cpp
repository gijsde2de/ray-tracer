#include "precomp.h" // include (only) this in every .cpp file

// -----------------------------------------------------------
// Initialize the game
// -----------------------------------------------------------
void Game::Init()
{
	camera = new Camera(vec3(0, 0, 0), vec3(0, 0, 1), 0);
	scene = new Scene();
}

void Game::HandleInput(float dt)
{
	if (keys.count(KEY_W)) camera->MoveLocal(vec3(dt, dt, dt));
	if (keys.count(KEY_S)) camera->MoveLocal(vec3(-dt, -dt, -dt));
	if (keys.count(KEY_Z)) camera->MoveGlobal(vec3(0, -dt, 0));
	if (keys.count(KEY_X)) camera->MoveGlobal(vec3(0, dt, 0));
	if (keys.count(KEY_A)) camera->MoveGlobal(camera->GetDirection().cross(camera->Up) * -dt);
	if (keys.count(KEY_D)) camera->MoveGlobal(camera->GetDirection().cross(camera->Up) * dt);
	dt /= 20;
	if (keys.count(KEY_E)) {
		vec3 dir = camera->GetDirection();
		camera->SetDirection(normalize(camera->GetDirection() + (camera->GetDirection().cross(camera->Up) * dt)));
	}
	if (keys.count(KEY_Q)) {
		vec3 dir = camera->GetDirection();
		camera->SetDirection(normalize(camera->GetDirection() + (camera->GetDirection().cross(camera->Up) * -dt)));
	}
}

// -----------------------------------------------------------
// Main game tick function
// -----------------------------------------------------------
void Game::Tick(float dt)
{
	screen->Clear(0);

	scene->Update();

	t.reset();
	scene->animationBVH->Construct(30, 10, true);
	float animationBuild = t.elapsed();

	scene->deformBVH->Refit();
	float refit = t.elapsed() - animationBuild;

	// top level bvh
	scene->Refit();
	float topLevelRefit = t.elapsed() - refit - animationBuild;

	HandleInput(dt);
	Trace();

	cout << "timings: animationBuild: " << animationBuild << "ms refit: " << refit << "ms Top Level refit " << topLevelRefit << "ms \n";
}

void Game::Shutdown() {}

void Game::Trace()
{
	for (int y = 0; y < SCRHEIGHT; y++) {
		for (int x = 0; x < SCRWIDTH; x++) {
			vec3 color = scene->Trace(camera->GetRay((float)x / SCRWIDTH, (float)y / SCRHEIGHT), 0);			
			screen->GetBuffer()[x + y * SCRWIDTH] =
				(Pixel)(color.x * 255.0f) << 16 & REDMASK |
				(Pixel)(color.y * 255.0f) << 8 & GREENMASK |
				(Pixel)(color.z * 255.0f) & BLUEMASK;
		}
	}
}