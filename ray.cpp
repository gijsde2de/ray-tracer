#include "precomp.h" // include (only) this in every .cpp file

Ray::Ray(vec3 o, vec3 d) {
	origin = o;
	direction = d;
	invDir = vec3(1 / direction.x, 1 / direction.y, 1 / direction.z);
	t = FLT_MAX;
}