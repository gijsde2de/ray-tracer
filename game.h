#pragma once
#include <unordered_set>

namespace Tmpl8 {

#define KEY_W				26
#define KEY_A				4
#define KEY_S				22
#define KEY_D				7
#define KEY_Q				20
#define KEY_E				8
#define KEY_R				21
#define KEY_F				9
#define KEY_T				23
#define KEY_G				10
#define KEY_Y				28
#define KEY_H				11
#define KEY_Z				29
#define KEY_X				27

class Surface;
class Camera;
class Scene;
class Game
{
public:
	void SetTarget( Surface* surface ) { screen = surface; }
	void Init();
	void Shutdown();
	void Tick( float deltaTime );
	void MouseUp( int button ) { /* implement if you want to detect mouse button presses */ }
	void MouseDown( int button ) { /* implement if you want to detect mouse button presses */ }
	void MouseMove( int x, int y ) { /* implement if you want to detect mouse movement */ }
	void KeyUp(int key) { keys.erase(key); }
	void KeyDown(int key) { keys.insert(key); }
	void Trace();
	void HandleInput( float deltaTime );
private:
	Surface* screen;
	Camera* camera;
	Scene* scene;
	unordered_set<int> keys;
	timer t;
};

}; // namespace Tmpl8