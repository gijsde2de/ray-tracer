#pragma once

namespace Tmpl8 {
	class Material
	{
	public:
		enum Type
		{
			DIFFUSE,
			MIRROR,
			SPECULAR
		};
		Type type;
		vec3 color;
		float specularity;
		Material(vec3 c, float s, Type t) { color = c; specularity = s; type = t; }
	private:

	};

}; // namespace Tmpl8