#pragma once

#define MAX_BOUNCES 10

namespace Tmpl8 {
	class Scene
	{
	public:
		struct Node
		{
			AABB bounds;
			unsigned int left;
			unsigned int right;
			BVH* bvh;
		};
		Scene();
		vec3 Trace(Ray r, uint depth);
		void Construct();
		void Refit();
		void Update();
		BVH* staticBVH;
		BVH* rigidBVH;
		BVH* deformBVH;
		BVH* animationBVH;
	private:
		vector<pair<Triangle, size_t>> triangles;
		vector<pair<Sphere, size_t>> spheres;
		vector<pair<Plane, size_t>> planes;
		vector<Material> materials;
		vector<Light> lights;

		vector<Node> nodes;
		vector<BVH*> bvhs;

		vec3 directIllumination(Intersection i);
		vec3 specularIllumination(Ray r, Intersection i);
		void loadObjFile(string filename, vec3 offset, BVH& bvh, float scale = 1);
		void reflect(Ray& r, Intersection i);

		AABB extendBounds(AABB a, AABB b);
		unsigned int bestMatch(vector<unsigned int>& list, unsigned int index);
		void erase(vector<unsigned int>& list, unsigned int idx);

		bool intersect(Ray& ray, Node& node);
		bool intersect(Ray& ray, Intersection& intersection, Node& node);

		int counter = 0;
	};

}; // namespace Tmpl8
