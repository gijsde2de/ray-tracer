#include "precomp.h" // include (only) this in every .cpp file

Triangle::Triangle(vec3 point, vec3 edge1, vec3 edge2)
{
	p1 = point;
	e1 = edge1;
	e2 = edge2;
	center = new float[3];
	center[0] = p1[0] + (e1[0] + e2[0]) / 3;
	center[1] = p1[1] + (e1[1] + e2[1]) / 3;
	center[2] = p1[2] + (e1[2] + e2[2]) / 3;
}

bool Triangle::Intersect(Ray& ray)
{
	vec3 p = cross(ray.direction, e2);
	float det = dot(e1, p);
	if (det > -FLT_EPSILON && det < FLT_EPSILON) return false;

	det = 1 / det;

	vec3 r = ray.origin - p1;
	float u = dot(r, p) * det;
	if (u < 0 || u > 1) return false;

	vec3 q = cross(r, e1);
	float v = dot(ray.direction, q) * det;
	if (v < 0 || u + v > 1) return false;

	float t = dot(e2, q) * det;
	if (t >= ray.t || t <= 0) return false;

	ray.t = t;
	return true;
}