#pragma once
#include "ray.h"

namespace Tmpl8 {
	class Camera
	{
	public:
		Camera(vec3 origin, vec3 direction, float fov);	
		void MoveLocal(vec3 delta);
		void MoveGlobal(vec3 delta);
		Ray GetRay(float u, float v);
		vec3 GetDirection() { return direction; }
		void SetDirection(vec3 d) { direction = d; calculateIntrinsics(); }
		vec3 Up;
	private:
		vec3 origin, direction;
		void calculateIntrinsics();
		vec3 p0, p1, p2;
		float fov;
	};

}; // namespace Tmpl8