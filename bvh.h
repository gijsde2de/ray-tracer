#pragma once

namespace Tmpl8 {
	struct Intersection
	{
		vec3 position;
		vec3 normal;
		int material;
	};

	class BVH
	{
	public:
		struct Node
		{
			AABB bounds;
			unsigned int leftFirst;
			unsigned int count; // If 0, not a leaf node
		};
		void Construct(uint maxDepth, uint bins = 0, bool reconstruct = false);
		void AddTriangle(Triangle t, size_t mat);
		void Refit();
		bool isLeftClosestChild(Ray r, Node n);
		Node* pool;
		Node* root;
		vector<pair<Triangle, size_t>> triangles;
		unsigned int* indices;
		bool Intersect(Ray& r, Intersection& intersection);
		bool Intersect(Ray& r);
		void SetTransformation(mat4 trans) { transform = trans; invTransform = mat4(trans); invTransform.invert(); }
		mat4 GetTransformation() { return transform; }
		AABB GetAABB();
	private:
		bool intersect(Ray& r, Node n, Intersection& intersection);
		bool intersect(Ray& r, Node n);
		AABB calculateBounds(Triangle t);
		void extendBounds(Triangle t, AABB& aabb);
		AABB extendBounds(AABB a, AABB b);
		void partition(Node& node, Node& left, Node& right, float split, int dim, bool sort);
		void subdivide(Node& n, uint maxDepth);
		vec3 getCenter(AABB aabb) { return vec3(aabb.max.x - (aabb.max.x - aabb.min.x) * 0.5f, aabb.max.y - (aabb.max.y - aabb.min.y) * 0.5f, aabb.max.z - (aabb.max.z - aabb.min.z) * 0.5f); }
		uint poolPtr;
		uint bins;
		mat4 transform;
		mat4 invTransform;
	};
}