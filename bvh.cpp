#include "precomp.h" // include (only) this in every .cpp file

void BVH::Construct(uint maxDepth, uint bins, bool reconstruct) {

	this->bins = bins;

	size_t size = triangles.size();

	if (!reconstruct) {
		pool = (Node*)MALLOC64((size * 2 - 1) * sizeof(Node));

		transform = mat4().identity();
		invTransform = mat4().identity();
	}

	// Initialize the indices
	indices = new unsigned int[size];
	for (size_t i = 0; i < size; i++) indices[i] = i;

	root = &pool[0];
	poolPtr = 2;

	(*root).leftFirst = 0;
	(*root).count = size;
	(*root).bounds = calculateBounds(triangles[0].first);
	for (size_t i = 1; i < size; i++) extendBounds(triangles[i].first, (*root).bounds);

	subdivide(*root, maxDepth);
}

void BVH::subdivide(Node& n, uint maxDepth) {
	if (n.count <= 3) return;
	if (maxDepth-- == 0) return;
	float sah = n.bounds.SurfaceArea() * n.count;
	float tempSah;
	float pos;
	int dim;
	Node& l = pool[poolPtr];
	Node& r = pool[poolPtr + 1];
	bool splitBetter = false;

	// Nasty problem, loop should start with i = 1 if we use binning.
	uint start = bins ? 1 : 0;
	uint cnt = bins ? (bins < n.count ? bins : n.count) : n.count;
	for (int i = 0; i < 3; i++) {

		float binSize = (n.bounds.max[i] - n.bounds.min[i]) / (float)cnt;

		for (size_t j = start; j < cnt; j++) {
			float splitPos;
			if (bins)
				splitPos = n.bounds.min[i] + binSize * j;
			else
				splitPos = triangles[indices[n.leftFirst + j]].first.center[i];

			partition(n, l, r, splitPos, i, false);
			tempSah = l.bounds.SurfaceArea() * l.count + r.bounds.SurfaceArea() * r.count;
			if (tempSah < sah) {
				sah = tempSah;
				dim = i;
				pos = splitPos;
				splitBetter = true;
			}
		}
	}

	if (!splitBetter) return;

	partition(n, l, r, pos, dim, true);

	// Make current node none leaf and increase poolPtr
	n.leftFirst = poolPtr;

	n.count = 0;
	poolPtr += 2;

	// Subdivide children if possible
	subdivide(l, maxDepth);
	subdivide(r, maxDepth);
}

void BVH::partition(Node& node, Node& left, Node& right, float split, int dim, bool sort) {
	left.bounds.min = left.bounds.max = vec3(0);
	right.bounds.min = right.bounds.max = vec3(0);

	// Partition triangles based on split position within the given dimension
	unsigned int l = 0;
	unsigned int r = node.count;

	for (unsigned int i = 0; l < r; ++i)
	{
		// Get the position of the triangle
		Triangle& tri = triangles[indices[node.leftFirst + (sort ? l : i)]].first;
		//float pos = tri.p1[dim] + (tri.e1[dim] + tri.e2[dim]) / 3;

		// Put the triangle in the correct bounding box
		if (tri.center[dim] < split)
		{
			if (l++ == 0) left.bounds = calculateBounds(tri);
			else extendBounds(tri, left.bounds);
		}
		else
		{
			if (r-- == node.count) right.bounds = calculateBounds(tri);
			else extendBounds(tri, right.bounds);

			// If asked, properly partition the indices
			if (sort)
			{
				unsigned int t = indices[node.leftFirst + l];
				indices[node.leftFirst + l] = indices[node.leftFirst + r];
				indices[node.leftFirst + r] = t;
			}
		}
	}

	// Now initialize the left and right child accordingly
	left.leftFirst = node.leftFirst;
	right.leftFirst = node.leftFirst + l;

	left.count = l;
	right.count = node.count - l;
}

AABB BVH::calculateBounds(Triangle t) {
	vec3 p2 = t.p1 + t.e1;
	vec3 p3 = t.p1 + t.e2;

	AABB aabb;
	aabb.min.x = min(min(t.p1.x, p2.x), p3.x);
	aabb.min.y = min(min(t.p1.y, p2.y), p3.y);
	aabb.min.z = min(min(t.p1.z, p2.z), p3.z);
	aabb.max.x = max(max(t.p1.x, p2.x), p3.x);
	aabb.max.y = max(max(t.p1.y, p2.y), p3.y);
	aabb.max.z = max(max(t.p1.z, p2.z), p3.z);

	return aabb;
}

void BVH::extendBounds(Triangle t, AABB& aabb) {
	vec3 p2 = t.p1 + t.e1;
	vec3 p3 = t.p1 + t.e2;
	aabb.min.x = min(min(min(aabb.min.x, t.p1.x), p2.x), p3.x);
	aabb.min.y = min(min(min(aabb.min.y, t.p1.y), p2.y), p3.y);
	aabb.min.z = min(min(min(aabb.min.z, t.p1.z), p2.z), p3.z);
	aabb.max.x = max(max(max(aabb.max.x, t.p1.x), p2.x), p3.x);
	aabb.max.y = max(max(max(aabb.max.y, t.p1.y), p2.y), p3.y);
	aabb.max.z = max(max(max(aabb.max.z, t.p1.z), p2.z), p3.z);
}

AABB BVH::extendBounds(AABB a, AABB b) {
	AABB aabb;
	aabb.min.x = min(a.min.x, b.min.x);
	aabb.min.y = min(a.min.y, b.min.y);
	aabb.min.z = min(a.min.z, b.min.z);
	aabb.max.x = max(a.max.x, b.max.x);
	aabb.max.y = max(a.max.y, b.max.y);
	aabb.max.z = max(a.max.z, b.max.z);

	return aabb;
}

float sumOfSquares(vec3 a, vec3 b) {
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

bool BVH::isLeftClosestChild(Ray r, Node n) {
	vec3 leftCenter = getCenter(pool[n.leftFirst].bounds);
	vec3 rightCenter = getCenter(pool[n.leftFirst + 1].bounds);

	float leftDistance = sumOfSquares(r.origin, leftCenter);
	float rightDistance = sumOfSquares(r.origin, rightCenter);

	if (leftDistance < rightDistance)
		return true;
	return false;
}

bool BVH::Intersect(Ray& r, Intersection& intersection) {
	
	r.Transform(invTransform);
	bool hit = intersect(r, *root, intersection);
	r.Transform(transform);
	
	return hit;
}

bool BVH::intersect(Ray& r, Node n, Intersection& intersection) {
	bool hit = false;

	if (!n.bounds.Intersect(r))
		return false;

	if (n.count != 0) {
		for (unsigned int i = n.leftFirst; i < n.leftFirst + n.count; i++) {
			pair<Triangle, size_t> t = triangles[indices[i]];
			if (t.first.Intersect(r)) {
				hit = true;
				intersection.position = r.origin + r.direction * r.t;
				intersection.normal = normalize(cross(t.first.e1, t.first.e2));
				intersection.material = t.second;
			}
		}
	}
	else {
		if (isLeftClosestChild(r, n)) {
			if (intersect(r, pool[n.leftFirst], intersection)) hit = true;
			if (intersect(r, pool[n.leftFirst + 1], intersection)) hit = true;
		}
		else {
			if (intersect(r, pool[n.leftFirst + 1], intersection)) hit = true;
			if (intersect(r, pool[n.leftFirst], intersection)) hit = true;
		}
	}

	return hit;
}

bool BVH::Intersect(Ray& r) {

	r.Transform(invTransform);
	bool hit = intersect(r, *root);
	r.Transform(transform);

	return hit;
}

bool BVH::intersect(Ray& r, Node n) {
	if (!n.bounds.Intersect(r)) return false;
	if (n.count != 0) {
		for (unsigned int i = n.leftFirst; i < n.leftFirst + n.count; i++)
			if (triangles[indices[i]].first.Intersect(r))
				return true;
	}
	else {
		return intersect(r, pool[n.leftFirst]) || intersect(r, pool[n.leftFirst + 1]);
	}

	return false;
}

void BVH::AddTriangle(Triangle t, size_t mat) {
	triangles.emplace_back(t, mat);
}

void BVH::Refit() {
	for (int i = poolPtr - 1; i >= 0; i--) {
		if (i != 1) {
			Node& n = pool[i];
			if (n.count == 0)
			{
				// It's not a leaf, make sure it fits its children
				AABB& l = pool[n.leftFirst + 0].bounds;
				AABB& r = pool[n.leftFirst + 1].bounds;
				n.bounds = extendBounds(l, r);
			}
			else
			{
				// It's a leaf so make sure it fits it's primitives
				n.bounds = calculateBounds(triangles[indices[n.leftFirst]].first);
				for (unsigned int j = 1; j < n.count; j++)
					extendBounds(triangles[indices[n.leftFirst + j]].first, n.bounds);
			}
		}
	}
}

AABB BVH::GetAABB()
{
	AABB r[4];
	r[0].max = (*root).bounds.max;
	r[0].min = (*root).bounds.min;

	r[1].max = vec3(r[0].min.x, r[0].max.y, r[0].max.z);
	r[1].min = vec3(r[0].max.x, r[0].min.y, r[0].min.z);
	r[2].max = vec3(r[0].max.x, r[0].min.y, r[0].max.z);
	r[2].min = vec3(r[0].min.x, r[0].max.y, r[0].min.z);
	r[3].max = vec3(r[0].max.x, r[0].max.y, r[0].min.z);
	r[3].min = vec3(r[0].min.x, r[0].min.y, r[0].max.z);

	r[0].max = (vec4(r[0].max, 1) * transform).xyz;
	r[0].min = (vec4(r[0].min, 1) * transform).xyz;
	r[1].max = (vec4(r[1].max, 1) * transform).xyz;
	r[1].min = (vec4(r[1].min, 1) * transform).xyz;
	r[2].max = (vec4(r[2].max, 1) * transform).xyz;
	r[2].min = (vec4(r[2].min, 1) * transform).xyz;
	r[3].max = (vec4(r[3].max, 1) * transform).xyz;
	r[3].min = (vec4(r[3].min, 1) * transform).xyz;

	r[0] = extendBounds(r[0], r[1]);
	r[0] = extendBounds(r[0], r[2]);
	r[0] = extendBounds(r[0], r[3]);

	return r[0];
}