#include "precomp.h"

float AABB::SurfaceArea()
{
	vec3 dims = max - min;
	return (dims.x * dims.y + dims.y * dims.z + dims.z * dims.x) * 2;
}

bool AABB::Intersect(Ray& ray)
{
	vec3 invD = ray.invDir;

	float tmin = -FLT_MAX;
	float tmax = FLT_MAX;
	float t1;
	float t2;

	for (int dim = 0; dim < 3; dim++) {
		t1 = (min[dim] - ray.origin[dim]) * invD[dim];
		t2 = (max[dim] - ray.origin[dim]) * invD[dim];

		tmin = max(tmin, min(t1, t2));
		tmax = min(tmax, max(t1, t2));
	}

	return tmax >= tmin && tmax >= 0 && tmin < ray.t;
}