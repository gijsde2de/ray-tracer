#include "precomp.h" // include (only) this in every .cpp file

Sphere::Sphere(vec3 p, float r2)
{
	position = p;
	radius2 = r2;
}

bool Sphere::Intersect(Ray& ray)
{
	vec3 c = position - ray.origin;
	float t = dot(c, ray.direction);

	vec3 q = c - t * ray.direction;
	float p2 = dot(q, q);

	if (p2 > radius2) return false;

	t -= sqrtf(radius2 - p2);
	if (t >= ray.t || t <= 0) return false;

	ray.t = t;
	return true;
}

