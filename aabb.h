#pragma once

namespace Tmpl8 {
	class AABB
	{
	public:
		vec3 min;
		vec3 max;

		float SurfaceArea();
		bool Intersect(Ray& ray); // Does not modify ray.t, as opposed to all other primitives
	};
}