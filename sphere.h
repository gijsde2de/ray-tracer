#pragma once

namespace Tmpl8 {
	class Sphere
	{
	public:
		vec3 position;
		float radius2; // Radius squared

		bool Intersect(Ray& ray);
		Sphere(vec3 p, float r2);
	};
}